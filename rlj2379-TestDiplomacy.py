#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        arm = []
        s = "A Southlake Hold\n"
        diplomacy_read(s, arm)
        self.assertEqual(arm[0], ('A', 'Southlake', 'Hold'))


    def test_read_2(self):
        arm = []
        s = "C Austin Support B\n"
        diplomacy_read(s, arm)
        self.assertEqual(arm[0], ('C', 'Austin', 'Support', 'B'))


    def test_read_3(self):
        arm = [('C', 'Austin', 'Support', 'B')]
        s = "B Dallas Move Southlake\n"
        diplomacy_read(s, arm)
        self.assertEqual(arm[1], ('B', 'Dallas', 'Move', 'Southlake'))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([('A', 'Prague', 'Hold'), 
                            ('B', 'Barcelona', 'Move', 'Prague'),
                            ('C', 'London', 'Move', 'Prague'),
                            ('D', 'Paris', 'Support', 'B'),
                            ('E', 'Austin', 'Support', 'A'),
                            ('F', 'Amsterdam', 'Move', 'Paris')])
        self.assertEqual(v, [('A', 'Prague'), ('B', '[dead]'), ('C', '[dead]'), ('D', '[dead]'), ('E', 'Austin'), ('F', '[dead]')])

    def test_eval_2(self):
        v = diplomacy_eval([('A', 'Lahti', 'Hold')])
        self.assertEqual(v, [('A', 'Lahti')])

    def test_eval_3(self):
        v = diplomacy_eval([('A', 'Plovdiv', 'Hold'),
                            ('B', 'Dnipro', 'Hold')])
        self.assertEqual(v, [('A', 'Plovdiv'), ('B', 'Dnipro')])

    # -----
    # print
    # -----
    
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [("A", "Plovdiv"), ("B", "Dnipro")])
        self.assertEqual(w.getvalue(), "A Plovdiv\nB Dnipro\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [("A", "Lahti")])
        self.assertEqual(w.getvalue(), "A Lahti\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [("A", "Prague"), ("B", "[dead]"), ("C", "[dead]"), ("D", "[dead]"), ("E", "Austin"), ("F", "[dead]")])
        self.assertEqual(w.getvalue(), "A Prague\nB [dead]\nC [dead]\nD [dead]\nE Austin\nF [dead]\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Prague Hold\nB Barcelona Move Prague\nC London Move Prague\nD Paris Support B\nE Austin Support A\nF Amsterdam Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Prague\nB [dead]\nC [dead]\nD [dead]\nE Austin\nF [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Lahti Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Lahti\n")

    def test_solve_3(self):
        r = StringIO("A Plovdiv Hold\nB Dnipro Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Plovdiv\nB Dnipro\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
